#!/bin/bash

export LC_NUMERIC="en_US.UTF-8"
source /opt/Xilinx/Vitis_HLS/2022.2/settings64.sh

cd ~/Xilinx
/opt/Xilinx/Vitis_HLS/2022.2/bin/vitis_hls >/dev/null &

