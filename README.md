# HLS Setup abd tools

## Xilinx setup

- Install dependencies first!!!

```sh
sudo apt install libtinfo5 libncurses5
```

- Install Xilinx to `/opt/Xilinx`
- Copy start scripts to `~/Xilinx`
  - `vitis_hls.sh`
  - `vitis.sh`
  - `vivado.sh`

- Note in the `.sh` files:
  - Always source the settings file before run. But don't source it in `.bashrc`, because it gonna mess up the c compilers for other apps.
  - Always start vivado/vitis from some separate Xilinx folder, because it creates a lot of log files.

## Git compatible folder architecture

```sh
📦_project_name
 ┣ 📂hls # Vitis HLS design src files
 ┃ ┗ 📂_ip_name
 ┃ ┃ ┗ 📂src
 ┃ ┃ ┃ ┗ 📜.gitkeep
 ┣ 📂sdk # Vitis software src files
 ┃ ┗ 📂_app_name
 ┃ ┃ ┗ 📜.gitkeep
 ┣ 📂src # Vivado IP integrator (ipi) files
 ┃ ┣ 📂constrs_1
 ┃ ┃ ┗ 📜_project_name.xdc
 ┃ ┗ 📂sources_1
 ┃ ┃ ┣ 📂bd
 ┃ ┃ ┃ ┗ 📂design_1
 ┃ ┃ ┃ ┃ ┣ 📂hdl
 ┃ ┃ ┃ ┃ ┃ ┗ 📜design_1_wrapper.vhd
 ┃ ┃ ┃ ┃ ┣ 📜design_1.bd
 ┃ ┃ ┃ ┃ ┗ 📜design_1_ooc.xdc
 ┃ ┃ ┗ 📜_custom_file.vhd
 ┣ 📜README.md
 ┣ 📜Xilinx.gitignore # use this gitignore to remove generated files under version control
 ┗ 📜_project_name.tcl # use this file to generate HW project from scratch
```

## Vitis HLS IP setup

- Open Vitis HLS
- Create Project
  - Name project with `*_ip` (custom convention)
  - Select location: `hls_ips`
  ![Create HLS project](readme/13.png)
  - Select HW part to synthetise on
  ![Create HLS project](readme/14.png)

## Vivado HW project setup from scratch

- Create folders:
  - <project_name>
  - <project_name>/src

- Open Vivado:
  - Create Project
  - Set `Project name` to `<project_name>` 
  - Set `Project location` to `<project_name>` folder
  ![Create vivado project 1](readme/01.png)
  ![Create vivado project 2](readme/02.png)

- Create Block Design:
  - Leave `Design name` as default `design_1`
  - Set `Directory` to `src`
  ![Create block design 1](readme/03.png)

- Create Wrapper for Block Design:
  - Right click on `Sources -> Design Sources -> design_1` and select `Create HDL Wrapper...`

- When Block Design is done, write the project configuration to a TCL file:
  - File -> Project -> Write TCL
  - Set `Output File` to `<project_name>/<project_name>.tcl`
  - Uncheck: `Copy sources to new project`
  ![Write tcl file](readme/04.png)

- Create .gitignore file from the Xilinx.gitignore template

### Export HW

- Run Synthesis -> Run Implementation -> Run Generate bitstream
- `File -> Export -> Export Hardware`
- Check `Include bitstream`
![Export HW 1](readme/06.png)
![Export HW 2](readme/07.png)

## Vitis SW project Setup

### Install drivers and debug tools

- Install Digilent Adept drivers for Zedboard
  - Adept Runtime Downloads: <https://lp.digilent.com/complete-adept-runtime-download>
  - Adept Utilities: <https://lp.digilent.com/complete-adept-utilities-download>

- Install `gtkterm` for serial

```sh
sudo apt install gtkterm
```

### Setup

- Launch Vitis IDE
- Create or switch to a new workspace (separate every Vitis (Eclipse) workspace)
- Create platform project
- Name the project with `*_platform` (custom convention)
![Create platform project](readme/08.png)
- Create application project on top of the created platform
![Select platform for application project](readme/09.png)
- Name application to `<app_name>`
![Create application project](readme/10.png)
- Put src files into `sdk/<app_name>` folder, and Link src files
![SW project link files](readme/11.png)
![SW project link files](readme/12.png)

## Reconstruct setup

### Clean workspace

- After checking out the project branch clean the workspace:

```sh
git clean -f -d -X
```

### Reconstruct Vitis HLS IPs

- Open Vitis HLS
- Run C Synthesis
- Run Export RTL

### Reconstruct Vivado HW project

- Open Vivado
- Open TCL Console:
  - Navigate to the project folder on the console (**important!**)
  - Run the tcl script:

```sh
source ./vivado_ipi.tcl
```
  
![Run tcl file](readme/05.png)

- Generate Bitstream
- File -> Export -> Export HW

### Reconstruct Vitis SW project

- Open Vitis IDE
- Create platform project from `.xsa` bitsream
- Create empty application project
- Link src files
