#!/bin/bash

export LC_NUMERIC="en_US.UTF-8"
export XILINXD_LICENSE_FILE=2100@atlas.itk.ppke.hu

source /opt/Xilinx/Vivado/2022.2/settings64.sh

cd ~/Xilinx
/opt/Xilinx/Vivado/2022.2/bin/vivado >/dev/null &

